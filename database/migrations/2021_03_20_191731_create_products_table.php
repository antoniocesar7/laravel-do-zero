<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            //$table->foreignId('category_id');//1ª versão
            $table->foreignId('category_id')->constrained();
            $table->string('name');
            $table->text('description');
            $table->boolean('exclusive');
            $table->timestamps();
            $table->softDeletes();

            //1ª Versão
            // $table->foreign('category_id')
            //       ->references('id')
            //       ->on('categories');

            //2ª Versão

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
